/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kurssitehtävät.pkg3;

import java.awt.List;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;
import olio.ohjelmointi.viikko.pkg8.FXMLDocumentController;

/**
 *
 * @author Joni Kettunen
 */
public class BottleDispenser {
    private DecimalFormat df2 = new DecimalFormat("0.00");
    private int bottles;
    // The array for the Bottle-objects
    public ArrayList<Bottle> bottle_array = new ArrayList<Bottle>(); 
    private double money;

    public double getMoney() {
        return money;
    }
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        // Initialize the arraylist
        // Add Bottle-objects to the array
        Bottle pepsi_small = new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8);
        Bottle pepsi_big = new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2);
        Bottle cola_small = new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0);
        Bottle cola_big = new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5);
        Bottle fanta_small = new Bottle("Fanta Zero", "Coca-Cola", 0.5, 1.95);
        bottle_array.add(pepsi_small);
        bottle_array.add(pepsi_big);
        bottle_array.add(cola_small);
        bottle_array.add(cola_big);
        bottle_array.add(fanta_small);
        bottle_array.add(fanta_small);
        }
    public void listBottles(){
        for(int i = 0;i<bottles;i++) {
            System.out.println(i+1 +  ". Nimi: " + bottle_array.get(i).getName() + " Hinta:" + bottle_array.get(i).getPrice());
        } 
    }
    
    public void addMoney(double x) {
        money  = money + x;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public String buyBottle(Bottle a) {
        String b;
        if(money> a.getPrice() && a.getSaldo() > 0){
        money = money - a.getPrice();
        b = ("Pullo " + a.getName() + " putosi koneesta! \n");
        return b;
        }
        else if(a.getSaldo() <= 0){
            b = ("Pullo " + a.getName() + " on loppunut koneesta! \n");
            return b;
        }
        else if(money < a.getPrice()){
            b = "Syötä lisää DogeCoineja \n";
            return b;
        }
        else{
            b = "Jotain meni vikaan! \n";
            return b;
        }
        }
    
    public void removeBottle(int i){
        bottle_array.remove(i);
        bottles--;
    }
    
    public void returnMoney() {        
        money = 0;
    }
}
