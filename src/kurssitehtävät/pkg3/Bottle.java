/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kurssitehtävät.pkg3;
import java.util.ArrayList;
/**
 *
 * @author Joni Kettunen
 */
public class Bottle {
    
    private String name;
    private String manuf;
    private double price;
    private double size;
    private double saldo = 10;
    
    public Bottle(String n,String m, double s, double p){
        name = n;
        manuf = m;
        price = p;
        size = s;
    }
    
    public Bottle(){
        
    }
    public String getName(){
        return name;
    }
        
    public String getManufacturer(){
        return manuf;
    }

    public double getPrice(){
        return price;
    }
    public double getSize(){
        return size;
    }
    public void setPrice(float p){
        price = p;
    }
    public void setSize(float s){
        size = s;
    }
    
    public void setName(String n){
        name = n;
    }
        
    public void setManufacturer(String m){
        manuf = m;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo() {
        saldo = saldo -1;
    }
    
    @Override
   public String toString(){
       String temp;
       temp = name + " " + size;
       return temp;
   }
    
}
