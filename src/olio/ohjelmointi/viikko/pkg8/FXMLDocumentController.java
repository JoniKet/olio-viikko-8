/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi.viikko.pkg8;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.input.DragEvent;
import kurssitehtävät.pkg3.Bottle;
import kurssitehtävät.pkg3.BottleDispenser;

/**
 *
 * @author Jönnsson
 */
public class FXMLDocumentController implements Initializable {
    
    BottleDispenser a = new BottleDispenser();
    Bottle latest;
    private String filename  = "kuitti.txt";
    
    @FXML
    private Label label;
    @FXML
    private Button bottleBuyButton;
    @FXML
    private Button insetDoshButton;
    @FXML
    private Slider dogeCoinAmountSlider;
    @FXML
    private Button returnDoshButton;
    @FXML
    private TextArea textBoxArea;
    @FXML
    private Button printReceiptButton;
    @FXML
    private ComboBox<Bottle> botlleChooserMenu;
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        int i = 0;
        botlleChooserMenu.getItems().addAll(a.bottle_array);
        // TODO
    }    

    
    
    /*Rahankäsittely alkaa */
    @FXML
    private void inserDogeCoin(ActionEvent event) {
        a.addMoney(dogeCoinAmountSlider.getValue());
        textBoxArea.appendText("Lisätty " + String.valueOf(dogeCoinAmountSlider.getValue()) + " verran dogeCoineja koneeseen" + "\n");
        textBoxArea.appendText("Koneessa yhteensä: " + String.valueOf(a.getMoney()) + " DogeCoineja \n" );
    }
    @FXML
    private void returnDogeCoin(ActionEvent event) {
        textBoxArea.appendText("Palautetaan: " + String.valueOf(a.getMoney()) + " verran DogeCoineja \n" );
        a.returnMoney();
    }
    
    /* Rahankäsittely loppuu*/

    
    @FXML
    private void buyBottle(ActionEvent event) {
        String temp;
        temp = a.buyBottle(botlleChooserMenu.getValue());
        latest = botlleChooserMenu.getValue();
        textBoxArea.appendText(temp);
        botlleChooserMenu.getValue().setSaldo();
    }


    @FXML
    private void printReceipt(ActionEvent event) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write("Dogespurdoautomaatti \n Ostettu pullo: " + latest.getName() + " " + String.valueOf(latest.getSize()) + " \n Pullon hinta: " + String.valueOf(latest.getPrice()) + " \n " );
        out.close();
        textBoxArea.appendText("Kuitti tulostettu! (tallennettu kuitti.txt");
        
    }


    
}
